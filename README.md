# WordPress Highly Extensible and Awesome Theme (WHEAT)

## Introduction

WordPress Hightly Extensible and Awesome Theme (WHEAT) is a theme template that can be used to create new and awesome WordPress themes. WHEAT is basically the infamous ["Underscores" theme](https://underscores.me/) with some additional bells and whistles thrown in for good measure. WHEAT uses Yarn version 2 for package management, and uses Yarn and Webpack to generate JavaScript and CSS bundles. You can execute `yarn start` from the command line in the theme's root directory to launch an in-memory development server with hot module reloading (HMR). The theme uses SASS to precompile CSS files, and Babel to compile ES6 JavaScript to a more cross-browser compatible ES5.

Execute the tasks below to create a new theme from this theme template. These instructions assume that you're running a WordPress website in Docker containers on your local development machine. If you're looking for a WordPress template project that runs in Docker containers, check out [WEED](https://gitlab.com/schlenker-public-repos/weed) (WordPress Environment for Easy Development).

## Installation and Setup Instructions

1. Download the Theme Template as a zip file.
2. Extract the zip file to the location of your choice on your hard drive.
3. Rename the folder from "theme-template-master" to "theme-template".
4. Copy the folder into /wp-content/themes/ in your WordPress project directory
5. Do a case-sensitive global find and replace in the theme-template folder, replacing all occurences of "theme-template" with the name of your theme (example: my-theme).
6. Do a case-sensitive global find and replace in the theme-template folder, replacing all occurences of `Theme Template` with the name of your theme (example: My Theme).
7. Do a case-sensitive global find and replace in the theme-template folder, replacing all occurences of `Theme_Template` with the name of your theme with the words in the theme name separated by an underscore (example: My_Theme).
8. Rename the theme-template folder to the name of your theme (example: my-theme).
9. Replace the theme thumbnail file called "screenshot.png" with your theme's thumbnail image. The name of the file must remain "screenshot.png", and the size of the image must be 880px X 660px.
10. CD into the new theme's root folder and execute the command `yarn install`. Make sure the installation passes.
11. Execute the command `yarn build`. Make sure the build passes.
12. In scripts/webpack.config.js, change `[Your domain here]` to your website's domain (example: mycoolsite.com).
13. Start the Docker containers.
14. Access the website in a browser using the domain name (not localhost) and log in.
15. Access the admin dashboard and navigate to Appearance --> Themes. Make sure the new theme is one of the themes listed.
16. Activate the new theme.
17. In a web browser, navigate to the website's home page. The content in the file front-page.php in the root directory of the new theme should be displayed.
18. In the terminal, execute `yarn start` in the root directory of the new theme. This will launch the in-memory development server. A browser window should automatically open and display your website home page at `https://localhost:4000`.
19. Edit the page header text in front-page.php and save the file. If hot module reloading (HMR) is working properly, the header text on the home page should update automatically, as soon as you save front-page.php. You shouldn't have to do a manual browser reload to see the change. If you have to do a manual browser reload to see the change you made, HMR isn't working properly and needs to be fixed.
20. Log out of the website.
21. To stop the in-memory dev server, type `Ctrl-C` in the terminal.
22. Shut down the Docker containers.
23. Commit the new theme to Git.
24. You new theme is now ready to be developed. Let your creativity flow!
