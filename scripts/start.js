'use strict';

const webpack = require('webpack');
const config = require('./webpack.config');

const clientCompiler = webpack(config);

/*
The following code block is what launches the Webpack Dev Server
and opens the website in a browser when you execute the command "yarn start".
*/
clientCompiler.watch(
  {
    noInfo: true,
    quiet: true,
  },
  (err, stats) => {
    if (err) return;
  }
);
